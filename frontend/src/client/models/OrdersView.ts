/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { OrderView } from './OrderView';

export type OrdersView = {
    data: Array<OrderView>;
    count: number;
};

