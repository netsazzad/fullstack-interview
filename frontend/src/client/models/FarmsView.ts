/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { FarmView } from './FarmView';

export type FarmsView = {
    data: Array<FarmView>;
    count: number;
};

